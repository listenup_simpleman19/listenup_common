from flask import g, request
from listenup_common.http_auth import HTTPTokenAuth
from listenup_common.api_response import ApiException
from listenup_common.logging import get_logger
from listenup_common.wrappers.users import verify_user_token
import jwt

token_auth = HTTPTokenAuth('Bearer')
token_optional_auth = HTTPTokenAuth('Bearer')

logger = get_logger(__name__)


@token_auth.verify_token
def verify_token(token):
    g.current_user_guid = None
    logger.info("Verifying token against users service")
    if token:
        logger.debug("Making call to users service to verify token")
        r = verify_user_token(token)

        if r and r.get("user_guid"):
            logger.info("Successfully verify user token")
            g.current_user_guid = r.get("user_guid")
            return True
        else:
            logger.debug("Verification failed with token got json: %s", r)
    else:
        logger.info("Token is not set")
    logger.info("Failed authentication of token")
    return False


@token_auth.error_handler
def token_error():
    """Return a 401 error to the client."""
    return ApiException(message="Authentication Required", status=401).to_response()


@token_optional_auth.verify_token
def verify_optional_token(token):
    logger.info("Verifying optional token")
    if not verify_token(token):
        logger.debug("Token failed verification but it was optional")
    return True


def get_current_token():
    if request.cookies.get('Bearer'):
        token = decode_token_no_verification(request.cookies.get('Bearer'), catch_exceptions=True)
    else:
        token = None
    return token


def decode_token_no_verification(token, catch_exceptions=True):
    if catch_exceptions:
        try:
            decoded = jwt.decode(token, algorithms=['HS256'], verify=False)
            return decoded
        except jwt.ExpiredSignatureError:
            logger.warn('Decoded an expired Token')
            return None
        except jwt.DecodeError:
            logger.warn('Tried to decode an invalid token')
            return None
    else:
        decoded = jwt.decode(token, algorithms=['HS256'], verify=False)
        return decoded
