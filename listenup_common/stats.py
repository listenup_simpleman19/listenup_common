import psutil
from flask import Blueprint
from listenup_common.api_response import ApiResult

stats = Blueprint('stats', __name__)


def cpu_percent(percpu=True):
    return psutil.cpu_percent(interval=1, percpu=percpu)


def cpu_count():
    return psutil.cpu_count()


def cpu_freq():
    return psutil.cpu_freq().current


def free_memory():
    return psutil.virtual_memory().available


def total_memory():
    return psutil.virtual_memory().total


def disk_stats():
    stats = []
    for disk in psutil.disk_partitions():
        stats.append({
            'mount_point': disk.mountpoint,
            'percent_used': psutil.disk_usage(disk.mountpoint).percent,
            'total': psutil.disk_usage(disk.mountpoint).total,
            'used': psutil.disk_usage(disk.mountpoint).used,
        })
    return stats


@stats.route('/stats')
def all_stats():
    return ApiResult(value={
        'cpu': {
            'percent_util': cpu_percent(False),
            'percent_util_per_cpu': cpu_percent(True),
            'cpu_count': cpu_count(),
        },
        'disks': disk_stats(),
        'memory': {
            'free': free_memory(),
            'total': total_memory(),
        }
    }).to_response()


@stats.route('/stat/disks')
def stat_disks():
    return ApiResult(value={
        'disks': disk_stats(),
    }).to_response()


@stats.route('/stat/memory')
def stat_memory():
    return ApiResult(value={
        'memory': {
            'free': free_memory(),
            'total': total_memory(),
        },
    }).to_response()


@stats.route('/stat/cpu')
def stat_cpu():
    return ApiResult(value={
        'cpu': {
            'percent_util': cpu_percent(False),
            'percent_util_per_cpu': cpu_percent(True),
            'cpu_count': cpu_count(),
        },
    }).to_response()
