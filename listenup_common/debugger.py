import sys


def start_debug(port):
    sys.path.append(".remote/pycharm-debug.egg")
    sys.path.append(".remote/pycharm-debug-py3k.egg")

    import pydevd
    pydevd.settrace('192.168.33.1', port=int(port), stdoutToServer=True, stderrToServer=True)
