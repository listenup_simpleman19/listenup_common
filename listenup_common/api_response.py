from flask import json, Response


class ApiResult(object):
    def __init__(self, value, status=200):
        self.value = value
        self.status = status

    def to_response(self):
        return Response(json.dumps(self.value),
                        status=self.status,
                        mimetype='application/json')


class ApiException(Exception):

    def __init__(self, message, status=400, errors=None):
        if errors:
            self.errors = errors
        else:
            self.errors = [message]
        self.message = message
        self.status = status

    def to_result(self):
        return ApiResult({
            'message': self.message,
            'errors': self.errors,
            'status': self.status,
        }, status=self.status)

    def to_response(self):
        return ApiResult({
            'message': self.message,
            'errors': self.errors,
            'status': self.status,
        }, status=self.status).to_response()


class ApiMessage(Exception):

    def __init__(self, message, status=400):
        self.message = message
        self.status = status

    def to_result(self):
        return ApiResult({
            'message': self.message,
            'status': self.status,
        }, status=self.status)

    def to_response(self):
        return ApiResult({
            'message': self.message,
            'status': self.status,
        }, status=self.status).to_response()
