from typing import List
from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_rss_url, _get_url_and_return_json
import os

logger = get_logger(__name__)

url_prefix = '/api/rss'
add_rss_feed_url = url_prefix + '/add'
all_rss_feeds_url = url_prefix + '/all'


def get_rss_feed_url(guid, include_podcasts=False):
    if include_podcasts:
        return url_prefix + "/" + guid + "/items"
    else:
        return url_prefix + "/" + guid


def get_rss_feed(guid, include_podcasts=False, internal=True):
    logger.info(f"Getting rss information for: {guid} include_items: {include_podcasts}")
    url = get_rss_url(internal) + get_rss_feed_url(guid, include_podcasts)
    return _get_url_and_return_json(url)

