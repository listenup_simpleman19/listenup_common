from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_files_url, DEFAULT_HEADERS
import json
import os

logger = get_logger(__name__)

url_prefix = '/api/files'
file_upload_url = url_prefix + '/upload_file'
internal_file_upload_url = url_prefix + '/upload_file'


def get_check_file_url(guid):
    return url_prefix + '/check/' + guid


def upload_file_to_url(filename, path_to_file, file_url):
    return _upload_file_to_url(filename, path_to_file, file_url)


def internal_upload_file_internal(filename, path_to_file, info, file_url):
    return _upload_file_with_info(filename, path_to_file, file_url, info)


def check_file(file_guid, base_url):
    url = base_url + get_check_file_url(file_guid)
    logger.debug("Checking for file existence at: %s", url)
    if requests.get(url).status_code == 200:
        return True
    else:
        return False


def download_file(url, file_location):
    # NOTE the stream=True parameter
    logger.info("Downloading file from: %s to: %s", url, file_location)
    r = requests.get(url, stream=True, headers=DEFAULT_HEADERS)
    if r.status_code == 200 or r.status_code == 201:
        logger.debug("Download from: %s returned a valid response code", url)
        with open(file_location, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
        logger.debug("Download from: %s succeeded", url)
        return file_location
    else:
        logger.error("Failed to download file from: %s", url)
        return None


def _upload_file_to_url(filename, path_to_file, upload_url):
    logger.info("Uploading filename: %s, filepath: %s, url: %s", filename, path_to_file, upload_url)
    local_filename = os.path.basename(path_to_file)
    files = {
        'file': (local_filename, open(path_to_file, 'rb'), {'Expires': '0'}),
        'filename': (None, filename, 'multipart/form-data'),
    }
    response = requests.put(upload_url, files=files)
    logger.info("Finished uploading file and received response of: %s", response.json())
    if response.status_code == 201 or response.status_code == 200:
        logger.info("Successfully uploaded file")
        return response.json()
    else:
        logger.error("Failed to upload file successfully: %s", response.content)
        return None


def _upload_file_with_info(filename, path_to_file, info, upload_url):
    logger.info("Uploading filename: %s, filepath: %s, url: %s", filename, path_to_file, upload_url)
    files = {
        'file': (filename, open(path_to_file, 'rb'), {'Expires': '0'}),
        'info': (None, json.dumps(info), 'application/json')
    }
    response = requests.put(upload_url, files=files)
    if response.status_code == 201 or response.status_code == 200:
        return response.json()
    else:
        logger.error("Failed to upload file successfully: %s", response.content)
        return None
