from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_marshaller_url, _get_url_and_return_json

logger = get_logger(__name__)

url_prefix = '/api/marshaller'
all_servers_url = url_prefix + '/servers'
all_servers_with_files_url = url_prefix + '/serversF'
get_upload_url_url = url_prefix + '/upload/url'
get_file_url_url = url_prefix + '/file'


def get_file_url(guid, internal=True):
    logger.debug("Getting file url from marshaller, internal: %s", str(internal))
    url = get_marshaller_url(internal) + get_file_url_url + '/' + guid
    return _get_url_and_return_json(url)


def get_upload_url(internal=True):
    logger.debug("Getting upload url from marshaller, internal: %s", str(internal))
    url = get_marshaller_url(internal) + get_upload_url_url
    return _get_url_and_return_json(url).get('url')


def get_all_servers(internal=True):
    logger.debug("Getting all servers from marshaller, internal: %s", str(internal))
    url = get_marshaller_url(internal) + all_servers_url
    return _get_url_and_return_json(url)

