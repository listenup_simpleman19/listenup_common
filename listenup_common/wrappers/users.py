from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_users_url, DEFAULT_HEADERS
import json
import os

logger = get_logger(__name__)

url_prefix = '/api/users'
verify_user_token_url = url_prefix + '/verify'


def verify_user_token(token):
    logger.info("Verifying user token")
    r = requests.post("/api/users/verify", json={'token': token}, headers=DEFAULT_HEADERS)
    if r.status_code == 200 and r.json() and r.json().get("user_guid"):
        logger.info("Successfully verify user token")
        return r.json()
    else:
        logger.debug("Verification failed with token got status code of: %s and json: %s", r.status_code, r.json())
    return None
