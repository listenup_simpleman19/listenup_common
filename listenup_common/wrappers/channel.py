from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_channel_url, DEFAULT_HEADERS, _get_url_and_return_json, get_default_headers_with_auth

logger = get_logger(__name__)

url_prefix = '/api/channel'
add_channel_url = url_prefix + '/add'
all_channel_url = url_prefix + '/all'
all_rss_channels_url = url_prefix + '/rss'
channel_by_url = url_prefix + '/url'


def _get_channel_guid_url(channel_guid):
    return url_prefix + '/' + channel_guid


def _get_channel_owners_url(channel_guid):
    return url_prefix + '/' + channel_guid + '/owners'


def _get_modify_channel_owners_url(channel_guid):
    return url_prefix + '/' + channel_guid + '/owner'


def get_channel_by_guid(guid: str, internal=True):
    logger.info("Getting channel by guid: %s", guid)
    url = get_channel_url(internal) + _get_channel_guid_url(guid)
    return _get_url_and_return_json(url)


def get_channel_by_url(rss_url: str, internal=True):
    logger.info("Getting channel by url: %s", rss_url)
    url = get_channel_url(internal) + channel_by_url
    r = requests.get(url=url, json={"feed_url": rss_url}, headers=DEFAULT_HEADERS)
    if r.status_code == 200 or r.status_code == 201:
        logger.debug("Found channel by url")
        return r.json()
    else:
        logger.debug("Failed to find channel for url: %s", rss_url)
        return None


def create_channel(json: dict, internal=True):
    logger.info("Creating channel")
    url = get_channel_url(internal) + add_channel_url
    r = requests.post(url, json=json, headers=get_default_headers_with_auth())
    if r.status_code == 200 or r.status_code == 201:
        logger.debug("Successfully created channel: %s", r.json()["guid"])
        return r.json()
    else:
        logger.error("Failed to create channel")
        return None


def update_channel(guid: str, json: dict, internal=True):
    logger.info("Updating channel %s", guid)
    url = get_channel_url(internal) + _get_channel_guid_url(guid)
    r = requests.post(url=url, json=json, headers=get_default_headers_with_auth())
    if r.status_code == 200 or r.status_code == 201:
        logger.debug("Successfully updated channel: %s", guid)
        return r.json()
    else:
        logger.error("Failed to update channel: %s", guid)
        return None
