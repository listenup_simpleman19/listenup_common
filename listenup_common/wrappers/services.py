from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_services_url, _get_url_and_return_json

logger = get_logger(__name__)

url_prefix = '/api/services'
nodes_url = url_prefix + '/nodes'
services_url = url_prefix + '/services'


def get_nodes(filter_query=None):
    logger.info("Getting nodes from services")
    url = get_services_url() + nodes_url + ('?' + filter_query if filter_query else "")
    return _get_url_and_return_json(url)


def get_services(filter_query=None):
    logger.info("Getting services from services")
    url = get_services_url() + services_url + ('?' + filter_query if filter_query else "")
    return _get_url_and_return_json(url)
