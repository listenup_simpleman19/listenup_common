from typing import List
from listenup_common import requests
from listenup_common.logging import get_logger
from . import get_podcast_url, DEFAULT_HEADERS, _get_url_and_return_json, get_default_headers_with_auth
import os

logger = get_logger(__name__)

url_prefix = '/api/podcast'
add_podcast_url = url_prefix + '/add'
all_podcasts_url = url_prefix + '/all'
podcast_info_list_url = url_prefix + '/list'


def get_upload_file_url(podcast_guid):
    return url_prefix + '/' + podcast_guid + '/upload'


def get_podcast_info_url(podcast_guid):
    return url_prefix + '/' + podcast_guid


def get_podcast_add_owner_url(podcast_guid):
    return url_prefix + '/' + podcast_guid + '/owner'


def create_podcast(title, description, author, language, channel_guid, pub_date, from_rss_feed=False, internal=True):
    logger.info("Creating podcast for channel: %s", channel_guid)
    create_url = get_podcast_url(internal) + add_podcast_url
    podcast_info = {
        'title': title,
        'description': description,
        'author': author,
        'language': language,
        'channel_guid': channel_guid,
        'from_rss_feed': from_rss_feed,
        'pub_date': pub_date.__str__(),
    }
    response = requests.post(url=create_url, json=podcast_info, headers=DEFAULT_HEADERS)
    if response.status_code == 200 or response.status_code == 201:
        logger.debug("Successfully created podcast for channel: %s", channel_guid)
        return response.json()
    else:
        logger.error("Failed to create podcast: %s for channel: %s", title, channel_guid)
        logger.error("Failed response was: %s", response.content)


def get_podcast_info(guid, include_channel=False, internal=True):
    logger.info("Getting podcast information for: %s", guid)
    url = get_podcast_url(internal) + get_podcast_info_url(guid)
    if include_channel:
        url += "?includeChannel=true"
    return _get_url_and_return_json(url)


def get_podcasts_info_by_guids(guids: List[str], internal=True):
    logger.info("Getting podcasts information by guids for: %s", guids)
    url = get_podcast_url(internal) + podcast_info_list_url
    podcasts_json = _get_url_and_return_json(url, json=guids)
    if podcasts_json:
        podcasts = {podcast.get("guid"): podcast for podcast in podcasts_json}
        return podcasts
    else:
        return None


def upload_podcast(filename, path_to_file, podcast_guid, internal=True):
    upload_url = get_podcast_url(internal) + get_upload_file_url(podcast_guid)
    logger.info("Uploading podcast filename: %s, filepath: %s, url: %s", filename, path_to_file, upload_url)
    local_filename = os.path.basename(path_to_file)
    files = {
        'file': (local_filename, open(path_to_file, 'rb'), {'Expires': '0'}),
        'filename': (None, filename, 'multipart/form-data'),
    }
    response = requests.put(upload_url, files=files)
    logger.info("Finished uploading file and received response of: %s", response.json())
    if response.status_code == 201 or response.status_code == 200:
        logger.info("Successfully uploaded file for podcast: %s", podcast_guid)
        return response.json()
    else:
        logger.error("Failed to upload file successfully: %s", response.content)
        return None


def add_owner_to_podcast(guid, user_guid, internal=True):
    logger.info("Adding owner: %s to podcast: %s", user_guid, guid)
    headers = get_default_headers_with_auth()
    url = get_podcast_url(internal) + get_podcast_add_owner_url(guid)
    r = requests.post(url, json={"user_guid": user_guid}, headers=headers)
    if r.status_code == 200 or r.status_code == 201:
        logger.info("Successfully added owner: %s to podcast: %s", user_guid, guid)
        return r.json()
    else:
        # TODO this may not be an issue, podcast could have already had person as owner
        logger.error("Failed to add owner: %s to podcast: %s", user_guid, guid)
        return None


def remove_owner_from_podcast(guid, user_guid, internal=True):
    logger.info("Removing owner: %s from podcast: %s", user_guid, guid)
    headers = get_default_headers_with_auth()
    url = get_podcast_url(internal) + get_podcast_add_owner_url(guid)
    r = requests.delete(url, json={"user_guid": user_guid}, headers=headers)
    if r.status_code == 200 or r.status_code == 201:
        logger.info("Successfully removed owner: %s from podcast: %s", user_guid, guid)
        return r.json()
    else:
        # TODO this may not be an issue, podcast could have already had person as owner
        logger.error("Failed to remove owner: %s from podcast: %s", user_guid, guid)
        return None