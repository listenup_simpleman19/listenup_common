from listenup_common.logging import get_logger
from . import get_listens_url, _get_url_and_return_json

logger = get_logger(__name__)

url_prefix = '/api/listens'
record_listen_url = url_prefix + '/record'
get_listen_count_url = url_prefix + '/podcast/'
get_user_location_url = url_prefix + '/location/'
top_listen_count_url = url_prefix + '/top/'


def get_top_listen_count(number_to_get: int, internal=True):
    logger.debug("Getting top: %s listen counts from listens, internal: %s", str(number_to_get), str(internal))
    url = get_listens_url(internal) + top_listen_count_url + str(number_to_get)
    return _get_url_and_return_json(url)
