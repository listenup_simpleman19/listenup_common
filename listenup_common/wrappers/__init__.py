import os
from typing import Optional
from listenup_common import requests
from listenup_common.logging import get_logger
from flask import request

logger = get_logger(__name__)

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0'
DEFAULT_HEADERS = {'User-Agent': USER_AGENT}

internal_lb = None
external_lb = None
internal_services = None
# Services are only internal
internal_marshaller = None
external_marshaller = None
internal_files = None
external_files = None
internal_podcast = None
external_podcast = None
internal_channel = None
external_channel = None
internal_images = None
external_images = None
internal_users = None
external_users = None
internal_rss = None
external_rss = None
internal_listens = None
external_listens = None


def get_default_headers_with_auth() -> dict:
    headers = DEFAULT_HEADERS
    if request:
        auth = request.headers.get("Authorization", None)
        headers['Authorization'] = auth
    return headers


def get_lb_url(internal=True):
    global internal_lb, external_lb
    if not internal_lb:
        internal_lb = os.environ.get("INTERNAL_LB")
    if not external_lb:
        external_lb = os.environ.get("LB")
    if internal:
        return internal_lb
    else:
        return external_lb


def get_services_url():
    global internal_services
    if not internal_services:
        internal_services = os.environ.get("INTERNAL_SERVICES")
    return internal_services


def get_marshaller_url(internal=True):
    global internal_marshaller, external_marshaller
    if not internal_marshaller:
        internal_marshaller = os.environ.get("INTERNAL_MARSHALLER")
    if not external_marshaller:
        external_marshaller = os.environ.get("LB")
    if internal:
        return internal_marshaller
    else:
        return external_marshaller


def get_files_url(hostname, internal=False):
    global internal_marshaller, external_marshaller
    if not internal_marshaller:
        internal_marshaller = os.environ.get("INTERNAL_FILES")
    if not external_marshaller:
        external_marshaller = os.environ.get("LB")
    if internal:
        return internal_marshaller
    else:
        return external_marshaller


def get_podcast_url(internal=True):
    global internal_podcast, external_podcast
    if not internal_podcast:
        internal_podcast = os.environ.get("INTERNAL_PODCAST")
    if not external_podcast:
        external_podcast = os.environ.get("LB")
    if internal:
        return internal_podcast
    else:
        return external_podcast


def get_channel_url(internal=True):
    global internal_channel, external_channel
    if not internal_channel:
        internal_channel = os.environ.get("INTERNAL_CHANNEL")
    if not external_channel:
        external_channel = os.environ.get("LB")
    if internal:
        return internal_channel
    else:
        return external_channel


def get_users_url(internal=True):
    global internal_users, external_users
    if not internal_users:
        internal_users = os.environ.get("INTERNAL_USERS")
    if not external_users:
        external_users = os.environ.get("LB")
    if internal:
        return internal_users
    else:
        return external_users


def get_images_url(internal=True):
    global internal_images, external_images
    if not internal_images:
        internal_images = os.environ.get("INTERNAL_IMAGES")
    if not external_images:
        external_images = os.environ.get("LB")
    if internal:
        return internal_images
    else:
        return external_images


def get_rss_url(internal=True):
    global internal_rss, external_rss
    if not internal_rss:
        internal_rss = os.environ.get("INTERNAL_RSS")
    if not external_rss:
        external_rss = os.environ.get("LB")
    if internal:
        return internal_rss
    else:
        return external_rss


def get_listens_url(internal=True):
    global internal_listens, external_listens
    if not internal_listens:
        internal_listens = os.environ.get("INTERNAL_LISTENS")
    if not external_listens:
        external_listens = os.environ.get("LB")
    if internal:
        return internal_listens
    else:
        return external_listens


def _get_url_and_return_json(url, valid_status_codes=None, headers=None, json=None) -> Optional[dict]:
    if not valid_status_codes:
        valid_status_codes = [200, 201]
    if not headers:
        headers = DEFAULT_HEADERS
    resp: requests.Response = requests.get(url, headers=headers, json=json)
    logger.debug("Got response back from url: %s response: %s", url, resp)
    if resp.status_code in valid_status_codes and resp.json():
        logger.debug("Got json back from url: %s response: %s", url, resp.json())
        return resp.json()
    else:
        logger.error("Failed to get content from url: %s and response: %s", url, resp)
        return None
