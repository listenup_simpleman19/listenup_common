import time
import uuid
from typing import List
from functools import wraps
from listenup_common.logging import get_logger
from listenup_common.api_response import ApiException
from flask import request
from flask import _request_ctx_stack, current_app, url_for as _url_for


logger = get_logger(__name__)


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time.time())


def url_for(*args, **kwargs):
    """url_for replacement that works even when there is no request context.
    """
    if '_external' not in kwargs:
        kwargs['_external'] = False
    reqctx = _request_ctx_stack.top
    if reqctx is None:
        if kwargs['_external']:
            raise RuntimeError('Cannot generate external URLs without a '
                               'request context.')
        with current_app.test_request_context():
            return _url_for(*args, **kwargs)
    return _url_for(*args, **kwargs)


def guid():
    return str(uuid.uuid4()).replace('-', '')


def app_key():
    return str(uuid.uuid4()).replace('-', '') + str(uuid.uuid4()).replace('-', '')


def require_json(required_attrs: List[str] = None):
    if required_attrs is None:
        required_attrs = []

    def require_json_decorator(func):
        @wraps(func)
        def require_json_func_wrapper(*args, **kwargs):
            logger.debug("Checking if json was sent on request")
            if request.json:
                logger.debug("Found json in request")
                content = request.json
                for attr in required_attrs:
                    if attr not in content:
                        logger.warn("%s.%s called missing: %s in request", func.__module__, func.__name__, attr)
                        raise ApiException(status=400, message="Missing: " + attr + " in json request")
                return func(*args, json=content, **kwargs)
            else:
                logger.info("%s.%s called without json in request", func.__module__, func.__name__)
                raise ApiException(message="Not a json request", status=400)
        return require_json_func_wrapper
    return require_json_decorator
