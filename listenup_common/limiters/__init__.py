import traceback
from flask import request, abort, g
from functools import wraps
from werkzeug.exceptions import NotFound
from listenup_common.logging import get_logger
import cachetools

# TODO Use Redis instead of TTL cache
logger = get_logger(__name__)
ttl_cache = cachetools.TTLCache(maxsize=1024, ttl=10 * 60)


def protected(fn=None, limit=10, minutes=60):
    """Bans IP after requesting a protected resource too many times.

    Prevents IP from making more than `limit` requests per `minutes` to
    the decorated route. Prevents enumerating secrets or tokens from urls or
    query arguments by blocking requests after too many 404 not found errors.
    """

    if not isinstance(limit, int):
        raise Exception('Limit must be an integer number.')
    if not isinstance(minutes, int):
        raise Exception('Minutes must be an integer number.')

    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            key = f'bruteforce-{request.endpoint}-{request.remote_addr}'
            count = get_count(key)
            try:
                if count > limit:
                    increment_counter(key)
                    logger.info('Request blocked by protected decorator.')
                    return '404', 404
            except:
                logger.error(traceback.format_exc())

            try:
                result = func(*args, **kwargs)
            except NotFound:
                try:
                    increment_counter(key)
                except:
                    pass
                raise

            if isinstance(result, tuple) and len(result) > 1 and result[1] == 404:
                try:
                    increment_counter(key)
                except:
                    pass

            return result

        return inner
    return wrapper(fn) if fn else wrapper


def rate_limited(fn=None, limit=20, methods=[], ip=True, user=True, minutes=1):
    """Limits requests to this endpoint to `limit` per `minutes`."""

    if not isinstance(limit, int):
        raise Exception('Limit must be an integer number.')
    if limit < 1:
        raise Exception('Limit must be greater than zero.')

    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            if not methods or request.method in methods:
                if ip:
                    key = get_ratelimit_counter_key(type='ip', for_methods=methods)
                    increment_counter(key)
                    count = get_count(key)
                    if count > limit:
                        abort(429)

                    if user and g.get('current_user_guid', None):
                        key = get_ratelimit_counter_key(type='ip', for_methods=methods)
                        increment_counter(key)
                        count = get_count(key)
                        if count > limit:
                            abort(429)

            return func(*args, **kwargs)

        return inner
    return wrapper(fn) if fn else wrapper


def get_ratelimit_counter_key(type=None, for_only_this_route=True, for_methods=None):
    if not isinstance(for_methods, list):
        for_methods = []
    if type == 'ip':
        key = request.remote_addr
    elif type == 'user':
        key = g.current_user_guid if g.get('current_user_guid', None) else None
    else:
        raise Exception('Unknown rate limit type: {0}'.format(type))
    route = ''
    if for_only_this_route:
        route = f'{request.endpoint}'
    return f'ratelimit-{type}-{",".join(for_methods)}-{key}{route}'


def increment_counter(key):
    try:
        ttl_cache[key] = get_count(key) + 1
    except:
        logger.error(traceback.format_exc())
        pass


def get_count(key):
    try:
        return int(ttl_cache.get(key) or 0)
    except:
        logger.error(traceback.format_exc())
        return 0
