from typing import Tuple, List
import json
import os

TAB = "    "

def generate_model(model, base_model=None) -> Tuple[str, List[str]]:
    generated_code = ""
    imports = []
    generated_code += "class " + model["name"] + "(db.Model):\n"
    genned_tuple = generate_fields(model["fields"])
    generated_code += genned_tuple[0]
    for gen_import in genned_tuple[1]:
        if gen_import not in imports:
            imports.append(gen_import)
    return generated_code, imports


def generate_fields(fields) -> Tuple[str, List[str]]:
    generated_code = ""
    generated_imports = []
    for field in fields:
        arguements = []
        field_type_arr = field["type"].split(":")
        field_type = field["type"].split(":")[0]
        generator = field.get("generator", None)
        if generator:
            generated_imports.append("from " + generator.split("#", 1)[0] + " import " +  generator.split("#", 1)[1])
        if len(field_type_arr) == 2:
            field_arg = "(" + field["type"].split(":")[1] + ")"
        else:
            field_arg = "()"
        arguements.append("db." + field_type + field_arg)
        generated_code += \
            TAB + field["name"] + " = db.Column("
        for arguement in arguements:
            generated_code += arguement + ","
        generated_code += ")\n"

    return generated_code, generated_imports


if __name__ == "__main__":
    files = ["scratch.json"]
    with open("base_models.py") as f:
        base_model = ""
        for line in f.readlines():
            base_model += line

    for generate_file in files:
        imports = ""
        models = ""
        with open(generate_file) as f:
           model_json = json.load(f)
           for model in model_json["models"]:
               model_tuple= generate_model(model)
               models += model_tuple[0]
               for genned_import in model_tuple[1]:
                   if genned_import not in imports:
                       imports += genned_import + "\n"
               models += "\n\n"
        filepath = model_json["package"] + os.sep + "models.py"
        if not os.path.exists(model_json["package"]):
            os.mkdir(model_json["package"])
        with open(filepath, 'w') as f:
            f.write(base_model
                    .replace("<imports>", imports)
                    .replace("<models>", models)
                    )

