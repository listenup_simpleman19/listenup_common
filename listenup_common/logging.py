import logging
import sys


def setup_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('(%(asctime)s):%(name)s:%(levelname)s:%(funcName)s--%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Kill default handler to allow for root logger to be the only handler that catches
    log = logging.getLogger('werkzeug')
    if log:
        log.handlers = []


def get_logger(name, log_level=logging.DEBUG):
    app_logger = logging.getLogger(name)
    app_logger.setLevel(log_level)
    return app_logger


# Auto configure root logger when imported
setup_logger()
