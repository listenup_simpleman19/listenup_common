from setuptools import setup

setup(
    name='Listenup-Common',
    description='Common classes and functions',
    version='0.38',
    packages=['listenup_common', 'listenup_common.wrappers', 'listenup_common.codegen', 'listenup_common.limiters'],
    install_requires=[
        'flask',
        'bcrypt',
        'flask-sqlalchemy',
        'python-etcd',
        'docker',
        'psutil',
        'requests',
        'urllib3',
    ]
)
