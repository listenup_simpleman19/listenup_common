FROM python:3.7-slim as builder

RUN apt-get update && apt-get install -y gcc

ARG WHEELHOUSE=/tmp/.wheels
ARG APP_ROOT=/usr/src/app

RUN mkdir -p ${APP_ROOT}
RUN mkdir -p ${WHEELHOUSE}
WORKDIR ${APP_ROOT}

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV COMMON_VERSION $SERVICE_VERSION
ENV WHEELHOUSE $WHEELHOUSE

COPY . ${APP_ROOT}
RUN pip install -r requirements.txt

RUN ./mkwheel_docker

FROM python:3.7-slim

RUN apt-get update && apt-get install -y gcc

ARG APP_ROOT=/usr/src/app

RUN mkdir -p ${APP_ROOT}
RUN mkdir -p ${APP_ROOT}/.wheels
WORKDIR ${APP_ROOT}

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV COMMON_VERSION $SERVICE_VERSION

ARG APP_ROOT=/usr/src/app

COPY --from=builder /tmp/.wheels/ /usr/src/app/.wheels/
COPY requirements_common.txt /tmp
RUN pip install --find-links .wheels --no-cache-dir "Listenup_Common==$COMMON_VERSION"
RUN pip install -r /tmp/requirements_common.txt