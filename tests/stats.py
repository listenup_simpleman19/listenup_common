import unittest
from listenup_common.stats import cpu_freq, cpu_count, cpu_percent, free_memory, total_memory, disk_stats


class StatsTest(unittest.TestCase):

    def test_cpu(self):
        print(cpu_freq())
        self.assertIsNotNone(cpu_freq())
        self.assertGreater(cpu_freq(), 0)
        print(cpu_count())
        self.assertIsNotNone(cpu_count())
        self.assertGreater(cpu_count(), 0)
        print(cpu_percent())
        self.assertIsNotNone(cpu_percent())

    def test_memory(self):
        print(total_memory())
        self.assertIsNotNone(total_memory())
        self.assertGreater(total_memory(), 0)
        print(free_memory())
        self.assertIsNotNone(free_memory())
        self.assertGreater(free_memory(), 0)

    def test_disk(self):
        print(disk_stats())
        self.assertIsNotNone(disk_stats())
        self.assertGreater(len(disk_stats()), 0)
